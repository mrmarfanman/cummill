<?php

namespace App\EventListener;

use App\Entity\User;
use App\Event\MarkdownCacheEvent;
use App\Event\MarkdownInitEvent;
use App\Markdown\AppExtension;
use League\CommonMark\Extension\Strikethrough\StrikethroughExtension;
use League\CommonMark\Extension\Table\TableExtension;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Set up the CommonMark and HTML Purifier libraries with all the default stuff
 * that we want everywhere.
 */
final class MarkdownListener implements EventSubscriberInterface {
    /**
     * @var AppExtension
     */
    private $appExtension;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(
        AppExtension $appExtension,
        TokenStorageInterface $tokenStorage
    ) {
        $this->appExtension = $appExtension;
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents(): array {
        return [
            MarkdownInitEvent::class => ['onMarkdownInit'],
            MarkdownCacheEvent::class => ['onMarkdownCache'],
        ];
    }

    public function onMarkdownInit(MarkdownInitEvent $event): void {
        $environment = $event->getEnvironment();
        $environment->addExtension($this->appExtension);
        $environment->addExtension(new StrikethroughExtension());
        $environment->addExtension(new TableExtension());
        $environment->mergeConfig([
            'html_input' => 'escape',
            'max_nesting_level' => 25,
        ]);

        $event->addHtmlPurifierConfig([
            // Convert non-link URLs to links.
            'AutoFormat.Linkify' => true,
            // Disable cache
            'Cache.DefinitionImpl' => null,
            // Add rel="nofollow" to outgoing links.
            'HTML.Nofollow' => true,
            // Disable embedding of external resources like images.
            'URI.DisableExternalResources' => false,
        ]);

        if ($this->shouldOpenExternalLinksInNewTab()) {
            $event->addHtmlPurifierConfig(['HTML.TargetBlank' => true]);
        }
    }

    public function onMarkdownCache(MarkdownCacheEvent $event): void {
        if ($this->shouldOpenExternalLinksInNewTab()) {
            $event->addToCacheKey('open_external_links_in_new_tab');
        }
    }

    private function shouldOpenExternalLinksInNewTab(): bool {
        $token = $this->tokenStorage->getToken();
        $user = $token ? $token->getUser() : null;

        return $user instanceof User && $user->openExternalLinksInNewTab();
    }
}
